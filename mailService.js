const nodemailer = require('nodemailer');
require('dotenv').config();

console.log("Creating nodemailer transport...")
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    provider: 'gmail',
    port: 465,
    secure: true,
    auth: {
      user: process.env.EMAIL, // Enter here email address from which you want to send emails
      pass: process.env.PASS // Enter here password for email account from which you want to send emails
    },
    tls: {
    rejectUnauthorized: false
    }
  });

  module.exports = transporter;