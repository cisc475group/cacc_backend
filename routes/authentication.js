const express = require('express'),
    authenticationController = require('../controllers/authentication'),
    passport = require('../security/passport');
const authRoutes = express.Router();

// /api/auth/register
authRoutes.post('/register', passport.requireAuth, authenticationController.register);

// /api/auth/login
authRoutes.post('/login', authenticationController.login);

module.exports = authRoutes; 