const express = require("express"),
    adminsController = require('../controllers/admins'),
    passport = require('../security/passport');
const adminsRoutes = express.Router();


// /api/admins/
adminsRoutes.get("", passport.requireAuth, adminsController.getAllAdmins);

// /api/admins/create_table
// ONLY CALL THIS WHRN CREATE TABLE
adminsRoutes.get("/create_table", passport.requireAuth, adminsController.createTable); 

// /api/admins/getAdminByEmail/:email
adminsRoutes.get('/getAdminByEmail/:email', passport.requireAuth, adminsController.getAdminByEmail)
// /api/admins/:id
// DELETE ADMIN
adminsRoutes.delete('/:id', passport.requireAuth, adminsController.deleteAdmins);


// /api/admins/protected
// test the protected routes
adminsRoutes.get('/protected', passport.requireAuth, function(req, res) {
    res.json({ msg: 'Congrats! You are seeing this because you are authorized'});
});

module.exports = adminsRoutes;