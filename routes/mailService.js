const { response } = require("express");

const express = require("express");
        mailServiceController = require("../controllers/mailService");
const mailServiceRoutes = express.Router();


mailServiceRoutes.post('/send', mailServiceController.sendEmail);


module.exports = mailServiceRoutes;