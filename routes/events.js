const express = require('express'),
    eventsController = require('../controllers/events'),
    passport = require('../security/passport');
const eventsRoutes = express.Router();

// /api/events/create_table
eventsRoutes.post('/create_table', passport.requireAuth, eventsController.createTable);

// /api/events/createEvent
eventsRoutes.post('/createEvent', passport.requireAuth, eventsController.createEvent);

// /api/events/:{event_Id}
eventsRoutes.get('/:event_Id', eventsController.getEventById);

// /api/events/
eventsRoutes.get('/', eventsController.getAllEvents);

// /api/events/category/:{category}
eventsRoutes.get('/category/:category', eventsController.getEventByCategory);   

// /api/events/title/:{title}
eventsRoutes.get('/title/:title', eventsController.getEventByTitle);

// /api/events/search/:title
eventsRoutes.get('/search/:title', eventsController.searchEventByTitle);

// /api/events/delete/:id
eventsRoutes.delete('/delete/:id', passport.requireAuth, eventsController.deleteEventById);

// /api/events/edit/:id
eventsRoutes.put('/edit/:id', passport.requireAuth, eventsController.editEventById);

module.exports = eventsRoutes;
      