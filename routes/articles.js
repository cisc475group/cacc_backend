const express = require('express'),
    articlesController = require ('../controllers/articles'),
    passport = require('../security/passport');
const articlesRoutes = express.Router();

// /api/articles/create_table
articlesRoutes.post('/create_table', passport.requireAuth, articlesController.createTable);

// api/articles/createArticle
articlesRoutes.post('/createArticle', passport.requireAuth, articlesController.createArticle);

// /api/articles/
articlesRoutes.get('/', articlesController.getAllArticles);

// /api/articles/category/:category
articlesRoutes.get('/category/:category', articlesController.getArticlesByCategory);

// /api/articles/title/:title
articlesRoutes.get('/title/:title', articlesController.getArticleByTitle);

// /api/articles/:id
articlesRoutes.get('/:id', articlesController.getArticleById);

// /api/articles/delete/:id
articlesRoutes.delete('/delete/:id', passport.requireAuth, articlesController.deleteArticleById);

// /api/articles/edit/:id
articlesRoutes.put('/edit/:id', passport.requireAuth, articlesController.editArticleById);



module.exports = articlesRoutes;