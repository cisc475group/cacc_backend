const express = require('express');
const headerMenuController = require('../controllers/headerMenu'),
    passport = require('../security/passport');
const headerMenuRoutes = express.Router();


// /api/headerMenu/
headerMenuRoutes.get('/', headerMenuController.getAllHeadermenu);

// /api/headerMenu/create_table
// ONLY CALL THIS WHEN CREATE TABLE
headerMenuRoutes.get('/create_table', passport.requireAuth, headerMenuController.createTable);
 
// api/headerMenu/add_menu
headerMenuRoutes.post('/add_menu', passport.requireAuth, headerMenuController.addMenu);

// api/headerMenu/update_by_id
headerMenuRoutes.put('/update_by_id', passport.requireAuth, headerMenuController.updateById);

// api/headerMenu/delete_by_id 
headerMenuRoutes.delete('/delete_by_id', passport.requireAuth, headerMenuController.deleteById);



 

module.exports = headerMenuRoutes;