const adminRole = {
    Admin: "admin",
    ClubAdmin: "club admin"
}

exports.authorizeAdminRole = function(role, edittingCategory){
    console.log(`Admin of ${role} request Api for ${edittingCategory}...`);
    if (role === edittingCategory || role === adminRole.Admin){
        return true;
    }
    return false;
}