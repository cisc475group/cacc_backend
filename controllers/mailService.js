const transporter = require('../mailService');


exports.sendEmail = async function(req, res){
    try{
        let senderName = req.body.contactFormName;
        let senderEmail = req.body.contactFormEmail;
        let messageSubject = req.body.contactFormSubjects;
        let messageText = req.body.contactFormMessage;
        let copyToSender = req.body.contactFormCopy;

        let mailOptions = {
            to: 'cacc_de@hotmail.com', // Enter here the email address on which you want to send emails from your customers
            from: senderName,
            subject: messageSubject,
            text: messageText,
            replyTo: senderEmail
        };
        if (copyToSender) {
            //mailOptions.to.push(senderEmail);
          }

        const response = await new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, function (error, results) {
                if (error) {
                  reject(new Error(error.message));
                }
                console.log(results);
                res.status(200).send({success: "email sent"});
                resolve(results);
              })
        })
  
    }catch (err){
        if (err){
            console.log(err);
            res.status(500).send({error: "error occured"});
        }
    }
}