const mysqlDb = require("../dbService");
const _ = require('lodash');
const patha = require('path');
const fs = require("fs");
const Joi = require('joi');
const dotenv = require('dotenv');
dotenv.config();

const hostname = process.env.PORT ? "https://caccback.herokuapp.com/" : 
  'http://localhost:3000/';
exports.createTable = function (req, res) {
  let sql = "CREATE TABLE FILE(id int AUTO_INCREMENT, name VARCHAR(255), size INT(4), path VARCHAR(255), type VARCHAR(255), category VARCHAR(255),  PRIMARY KEY(id))";
  mysqlDb.query(sql, (err, result) => {
    if (err) throw err;
    console.log(result);
    res.send('files table created..');
  });
}


exports.uploadSingleFile = async function (req, res) {
  try {
    if (!req.files) {
      console.log(req);
      res.send({
        status: false,
        message: "No file uploaded",
      });
    } else {
      console.log(req.files);
      let image = req.files.files;
      const name = image.name.replace(/\s/g, ""),
        mimetype = image.mimetype,
        size = image.size;
      const category = req.body.category;
      const path = "uploads/" + image.name;
      console.log(path);
      const query = `INSERT INTO FILE (name, size, path, type, category) VALUES('${name}',  ${size}, '${path}','${mimetype}', '${category}');`;
      mysqlDb.query(query, (err, results) => {
        //if (err) reject(new Error(err.message));
        if (err)
          console.log(err);
        //resolve(results);
      });

      image.mv("./uploads/" + name);
      res.send({
        status: true,
        message: "File is uploaded",
        data: {
          name: name,
          mimetype: mimetype,
          size: size,
        },
        category: category,
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }

}

exports.upload = async function (req, res) {
  console.log(req.files);
  try {
    if (!req.files) {
      res.send({
        uploaded: false,
        message: 'No file uploaded'
      });
    } else {
      let image = req.files.upload;
      const name = image.name.replace(/\s/g, ""),
        mimetype = image.mimetype,
        size = image.size;


      const path = "uploads/" + image.name;
      const query = `INSERT INTO FILE (name, size, path, type, category) VALUES('${name}',  ${size}, '${path}','${mimetype}', 'article');`;
      mysqlDb.query(query, (err, results) => {
        //if (err) reject(new Error(err.message));
        if (err)
          console.log(err);
        //resolve(results);
      });
      const cccc = hostname + 'static/' + name;
      image.mv('./uploads/' + name);
      res.send({
        uploaded: true,
        url: cccc,
      });
    }
  } catch (err) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    }
  }
}


exports.uploadMultipleFile = async function (req, res) {
  try {
    if (!req.files) {
      res.send({
        status: false,
        message: 'No file uploaded'
      });
    } else {
      var data = [];

      const files = req.files.files;
      files.forEach(file => {
        const name = file.name.replace(/\s/g, ""),
          mimetype = file.mimetype,
          size = file.size;

        const path = "./uploads/" + file.name;
        const category = req.body.category;
        const query = `INSERT INTO FILE (name, size, path, type, category) VALUES('${name}',  ${size}, '${path}','${mimetype}', '${category}');`;
        mysqlDb.query(query, (err, results) => {
          //if (err) reject(new Error(err.message));
          //if (err)
          //console.log(err);
          //resolve(results);
        });

        file.mv(path);

        res.send({
          status: true,
          message: "File is uploaded",
          data: {
            name: name,
            mimetype: mimetype,
            size: size,
          },
          category: category,
        });
      })


      // return response
      res.send({
        status: true,
        message: 'Files are uploaded',
        data: data
      });

    }
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
}

exports.getFilesByCategory = async function (req, res) {
  try {
    const response = await new Promise((resolve, reject) => {
        const sql = `SELECT * FROM FILE WHERE category = '${req.params.category}'`;
        mysqlDb.query(sql, (err, results) => {
          if (err) reject(new Error(err.message));
          resolve(results);
        });
      })
      .then(data => res.json({
        data: data
      }))
      .catch(err => console.log(err));
  } catch (err) {
    console.log(err);
  }
}

exports.getFileByName = async function(req, res) {
  try {
    const name = req.params.name;

    const schema = Joi.object({
      name: Joi.string().required(),
    });
    const result = schema.validate(req.params);
    if (result.error) {
      res.status(400).send({error: result.error.details[0].message});
      return;
    }

    const response = await new Promise((resolve, reject) => {
        const sql = `SELECT * FROM FILE WHERE name = '${name}'`;
        mysqlDb.query(sql, (err, results) => {
          if (err) reject(new Error(err.message));
          resolve(results);
        });
      })
      .then(data => {
        data.map(val => val.path = hostname+`static/${name}`);
        res.json({
        data: data
        }) 
      })
      .catch(err => console.log(err));
  } catch (err) {
    console.log(err);
  }
}

exports.getAllFiles = async function (req, res) {
  try {
    const response = await new Promise((resolve, reject) => {
        const sql = `SELECT * FROM FILE`;
        mysqlDb.query(sql, (err, results) => {
          if (err) reject(new Error(err.message));
          resolve(results);
        });
      })
      .then(data => {
        data.map(val => val.path = hostname + val.path);
        res.json({
          data: data
        })
      })
      .catch(err => console.log(err));
  } catch (err) {
    console.log(err);
  }
}

exports.uploadFileBase64 = async function (req, res) {
  const name = req.body.name,
      category = req.body.category,
      base64 = req.body.base64;
  const schema = Joi.object({
    name: Joi.string().required(),
    category: Joi.string().required(),
    base64: Joi.string().required()
  });
  const result = schema.validate(req.body);
  if (result.error) {
    res.status(400).send({error: result.error.details[0].message});
    return;
  }
  const base64Data = base64.replace(/^data:image\/png;base64,/, "");
  const path = `./uploads/${name}`;
  console.log(`${name}, ${path}, ${category}`);
  fs.writeFile(path, base64Data, "base64", async function(err){
    //res.status(400).send(err);
    if (err){
      res.status(400).send({
        "error": "Fail to upload file to disk"
      });
      return;
    }else{
      try{
        const response = await new Promise((resolve, reject)=> {
            const query = `INSERT INTO FILE (name, path, category) VALUES('${name}','${path}','${category}');`;

            mysqlDb.query(query, (err, results) =>{
                if (err) reject(new Error(err.message));
                resolve(results);
            });
        })
        .then(data => res.json({
          success: "upload and save image successfully",
          data : [
          {
            "name":name,
            "category":category,
            "path":path,
            "base64": base64
          }
          ]
        }))
        .catch(err => console.log(err));
        //res.send(response);
      } catch (error){
        console.log(error);
      }
    }
  })
  
  //res.status(200).send("success");
}

exports.deleteFileById = function(req,res){
  const id = req.params.id;
    const schema = Joi.object({
        id: Joi.string().required(),
    });
    const result = schema.validate(req.params);
    if (result.error) {
        res.status(400).send({error: result.error.details[0].message});
        return;
    }

    mysqlDb.query(`SELECT path FROM FILE WHERE id = '${id}'`, (err, results) => {
      if (err) {
        res.status(400).send({"error": "error occured"});
        return;
      }
      if (results.length >= 0){
        const sql = `DELETE FROM FILE WHERE id = '${id}'`;
        mysqlDb.query(sql, (err, resultss) => {
          if (err){
            res.status(400).send({"error": "error occured"});
            return;
          }

          if (resultss.affectedRows >= 1){
            fs.unlink(results[0].path, (err) => {
              if (err) {
                console.error(err)
                return
              }
            
              res.status(200).send({"success":"deleted the file"});
            })
          }else {
            res.status(205).send({"failed": "failed to delete"});
          }
        })
      }else {
        res.status(205).send({"fail":"can not find the file"})
      }
    })
}