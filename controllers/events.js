const mysqlDb = require('../dbService');
const Joi = require('joi');
const adminRouleAuth = require('../security/adminRoleAuth');

// create the events table
exports.createTable = function (req, res) {
    // create table


    let sql = `CREATE TABLE event (event_id int AUTO_INCREMENT PRIMARY KEY, content BLOB, title varchar(255), create_date DATETIME DEFAULT NULL,category varchar(255), date varchar(255), address varchar(255), time varchar(255), endTime varchar(255), views int(4) DEFAULT 0)`;
    mysqlDb.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result);
        res.send('evets table created..');
    });



    // create trigger
    /* var trigger = `CREATE TRIGGER events_trg BEFORE INSERT ON events FOR EACH ROW BEGIN IF inserting THEN :new.create_date := NOW(); END IF; END;`
     mysqlDb.query(trigger, (err, result) => {
         if (err) throw err;
         console.log(err);
         res.send('admins table created..');
     });*/
}

exports.createEvent = async function (req, res) {
    console.log(req.body);
    try {
        let content = req.body.content,
            title = req.body.title,
            category = req.body.category,
            date = req.body.date,
            address = req.body.address;
            time = req.body.time;
            endTime = req.body.endTime;
        
        if (!adminRouleAuth.authorizeAdminRole(req.body.role, category)){
            console.log(`The Access is Restricted for Admin of ${req.body.role}`);
            res.status(400).send({error: `The Access is Restricted for Admin of ${req.body.role}`});
            return;
        }

        const schema = Joi.object({
            content: Joi.string().required(),
            title: Joi.string().required(),
            category: Joi.string().required(),
            date: Joi.date().required(),
            address: Joi.string().required(),
            time: Joi.string().required(),
            endTime: Joi.string().required(),
            role: Joi.string().required()
        });
        const result = schema.validate(req.body);
        if (result.error) {
            console.log(result.error.details[0].message);
            res.status(400).send({error: result.error.details[0].message});
            return;
        } 
        const response = await new Promise((resolve, reject) => {
            mysqlDb.query(`SELECT * FROM event WHERE title = '${title}'`, (error, result) => {
                if (error) reject(new Error(error.message));

                if (result.length != 0){
                    console.log("the title is exist")
                    res.status(400).send({error: "the title is exist"})
                    return;
                }
                var sql = `INSERT INTO event (content, title, category, date, address, time, endTime)  VALUES ('${content}', '${title}', '${category}', '${date}', '${address}', '${time}', '${endTime}');`;

                    mysqlDb.query(sql, (err, results) => {
                        if (err)
                            reject(new Error(err.message));
                    });
                    mysqlDb.query('SELECT LAST_INSERT_ID() event_id', (err, result) => {
                        if (err)
                            reject(new Error(err.message));
                        resolve(result);
                    })
                });
            }).then(data => {
                res.json({
                    event_id: data[0].event_id,
                    content: content,
                    title: title,
                    category: category,
                    date: date,
                    address: address,
                    time: time,
                    endTime: endTime
                });
            })
            .catch(err => {
                res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
                console.log(err)
            });

    } catch (err) {
        if (err) {
            console.log(err);
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
        }
    }
}

exports.getAllEvents = async function (req, res) {
    try {
        const response = await new Promise((resolve, reject) => {
                var sql = `SELECT event_id, title, category,CONVERT(content USING utf8) content, date, address, time, endTime FROM event`;

                mysqlDb.query(sql, (err, results) => {
                    if (err)
                        reject(new Error(err.message));
                    resolve(results);
                });
            }).then(data => {
                var ress = []
                data.map(val => ress.push(val));
                res.json(ress);
            })
            .catch(err => {
                res.status(500).send(err);
                console.log(err)
            });
    } catch (err) {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        }

    }
}

exports.getEventByCategory = async function (req, res) {
    try {
        const category = req.params.category;
        console.log(category);
        if (!category) {
            res.send({
                status: false,
                message: "you should provide valid category when getting event",
            });
        }

        const reponse = await new Promise((resolve, reject) => {
            var sql = `SELECT event_id, title, CONVERT(content USING utf8) content FROM event WHERE category = '${category}'`

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(err);
                resolve(results);
            })
        }).then(data => res.json(data)).catch(err => {
            res.status(500).send(err);
            console.log(err)
        });

    } catch (err) {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        }
    }
}

exports.getEventById = async function (req, res) {
    try {
        const event_Id = req.params.event_Id;
        const response = await new Promise((resolve, reject) => {
                var sql = `SELECT event_id, title, category,CONVERT(content USING utf8) content, date, address,time, endTime FROM event WHERE event_Id = ${event_Id}`

                mysqlDb.query(sql, (err, results) => {
                    if (err)
                        reject(new Error(err.message));
                    resolve(results);
                });
            }).then(data => {
                res.json(data[0]);
            })
            .catch(err => {
                res.status(500).send(err);
                console.log(err)
            })

    } catch {
        if (err) {
            console.log(err);
            res.status(500).send(err);
        }
    }
}


exports.getEventByTitle = async function (req, res) {
    try {
        const title = req.params.title;

        const schema = Joi.object({
            title: Joi.string().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const reponse = await new Promise((resolve, reject) => {
            var sql = `SELECT event_id, title, category,CONVERT(content USING utf8) content, date, address, time, endTime FROM event WHERE title = '${title}'`

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(err);
                resolve(results);
            })
        }).then(data => res.json(data)).catch(err => {
            res.status(500).send(err);
            console.log(err)
        });

    } catch (err) {
        if (err) {
            res.status(500).send(err);
            console.log(err);
        }
    }
}

exports.searchEventByTitle = async function(req, res){
    try{
        const title = req.params.title;
        //console.log(title);
        const schema = Joi.object({
            title: Joi.string().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            res.status(400).send({error: result.error.details[0].message});
            return;
        }
        
        const searchTitle = `%${title}%`;
        //console.log(searchTitle);
        const response = await new Promise((resolve, reject) => {
            const sql = `SELECT event_id, title, category,CONVERT(content USING utf8) content, date, address, time, endTime FROM event WHERE title LIKE '${searchTitle}'`;
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message));
                resolve(results);
            });

        })
        .then(data => res.status(200).send(data))
        .catch(err => {
            console.log(err);
            res.status(500).send({error: err});
        });

    }catch(err){
        if (err){
            console.log(err);
            res.status(500).send({error: err});
        }
    }
}

exports.deleteEventById = async function(req, res){
    const id = req.params.id;
    const schema = Joi.object({
        id: Joi.string().required(),
    });
    const result = schema.validate(req.params);
    if (result.error) {
        res.status(400).send({error: result.error.details[0].message});
        return;
    }

    try {
        const response = await new Promise((resolve, reject) => {
           const query = `DELETE FROM event WHERE event_id = '${id}'`;
           console.log(query);
           mysqlDb.query(query, (err, results) => {
               if (err) reject(new Error(err.message));
               resolve(results);
           });
        })
        .then(data => data.affectedRows == 0 ? res.status(205).send({error: 'Item Does not exist'}) : res.status(200).send({success:' Item Deleted'}))
        .catch(err => res.status(400).send(`Error Occured`));
    } catch (error){
        console.log(error);
    }
}

exports.editEventById = async function(req, res){
    const content = req.body.content,
        title = req.body.title,
        category = req.body.category,
        date = req.body.date,
        address = req.body.address,
        time = req.body.time,
        endTime = req.body.endTime;
    
    if (!adminRouleAuth.authorizeAdminRole(req.body.role, category)){
        console.log(`The Access is Restricted for Admin of ${req.body.role}`);
        res.status(400).send({error: `The Access is Restricted for Admin of ${req.body.role}`});
        return;
    }

    const id = req.params.id;
    if (!id){
        res.send({
            status: false,
            message: "you should provide valid id when editing",
        });
    }

    const schema = Joi.object({
        content: Joi.string().required(),
        title: Joi.string().required(),
        category: Joi.string().required(),
        date: Joi.date().required(),
        address: Joi.string().required(),
        time: Joi.string().required(),
        endTime: Joi.string().required(),
        role: Joi.string().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        console.log(result.error.details[0].message);
        res.status(400).send({error: result.error.details[0].message});
        return;
    } 

    try{
        const response = await new Promise((resolve, reject) => {
            var sql = `UPDATE event SET content = '${content}', title = '${title}', category ='${category}', date = '${date}', address = '${address}', time = '${time}', endTime = '${endTime}' WHERE event_Id = ${id}`;

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(new Error(err.message));
            });
            mysqlDb.query('SELECT LAST_INSERT_ID() event_id', (err, result) => {
                if (err)
                    reject(new Error(err.message));
                resolve(result);
            })
        }).then(data => {
            res.json({
                //event_id: data[0].event_id,
                content: content,
                title: title,
                category: category,
                date: date,
                address: address,
                time: time,
                endTime: endTime
            });
        })
        .catch(err => {
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
            console.log(err)
        });
    } catch (err) {
        if (err) {
            console.log(err);
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
        }
    }
}