const Joi = require('joi');
const headerMenuSchema = require("../models/headerMenu");
const mysqlDb = require('../dbService');
const e = require('express');

const MAX_MENUS = 4;
// create table
exports.createTable = function (req, res) {
    const sql = "CREATE TABLE headerMenu(id int AUTO_INCREMENT, name VARCHAR(20), link VARCHAR(100), parent VARCHAR(20), PRIMARY KEY (id))";
    mysqlDb.query(sql, (err, result) => {
        if (err) {
            res.status(400).send({
                "error": `${err.message}`
            });
        } else {
            console.log(result);
            res.status(200).send({
                "success": "table created",
                "result": `${result}`
            })
        }
    });
}

//get AllheaderMenu
exports.getAllHeadermenu = async function (req, res) {
    try {
        const response = await new Promise((resolve, reject) => {
                const query = "SELECT * FROM headerMenu;";

                mysqlDb.query(query, (err, results) => {
                    if (err) reject(new Error(err.message));
                    resolve(results);
                });
            })
            .then(data => res.json({
                data: data
            }))
            .catch(err => console.log(err));
        //res.send(response);
    } catch (error) {
        console.log(error);
    }
}

// add a menu
exports.addMenu = function (req, res) {
    const name = req.body.name;
    const link = req.body.link;
    const parent = req.body.parent;

    // use Joi to check req validation
    const schema = Joi.object({
        name: Joi.string().min(2).max(20).required(),
        link: Joi.string().min(0).max(100).required(),
        parent: Joi.string().min(2).max(20).required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        res.status(205).send({error: result.error.details[0].message});
        return;
    }

    // check if exceed MAX_MENUS
    mysqlDb.query(`SELECT * FROM headerMenu WHERE parent = 'none'`, (err, results) => {
        if (err) {
            res.status(400).send({
                "error": "error occurred"
            });
        } else {
            //console.log(results);
            if (results.length >= MAX_MENUS && parent == "none") {
                res.status(205).send({
                    "failed": `the maximum numbers of header menus is ${MAX_MENUS}, there are ${results.length} menus already`
                });
            } else {
                // write query
                const sql = `INSERT INTO headerMenu (name, link, parent) VALUES('${name}', '${link}', '${parent}');`;
                mysqlDb.query(sql, (err, results) => {
                    if (err) {
                        res.status(400).send({
                            "error": "error occurred"
                        });
                    } else {
                        res.status(200).send({
                            "success": "menu added"
                        })
                    }
                });
            }
        }
    });
}

// update a menu by id
exports.updateById = function (req, res) {
    const id = req.body.id;
    const name = req.body.name;
    const link = req.body.link;
    const parent = req.body.parent;

    // use Joi to check req validation
    const schema = Joi.object({
        id: Joi.number().integer().required(),
        name: Joi.string().min(2).max(20).required(),
        link: Joi.string().min(0).max(100).required(),
        parent: Joi.string().min(2).max(20).required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        res.status(205).send({error: result.error.details[0].message});
        return;
    }

    // update
    const sql = `UPDATE headerMenu SET name = '${name}', link = '${link}', parent = '${parent}' WHERE id = ${id}`;
    mysqlDb.query(sql, (err, results) => {
        if (err) {
            res.status(400).send({
                "error": `error occurred`
            });
        } else {
            res.status(200).send({
                "success": `Id: ${id} menu has been updated`
            });
        }
    });
}

// delete by id
exports.deleteById = function (req, res) {
    const id = req.body.id;

    // use Joi to check req validation
    const schema = Joi.object({
        id: Joi.number().integer().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        res.status(205).send({error: result.error.details[0].message});
        return;
    }

    // delete
    const sql = `DELETE FROM headerMenu WHERE id = ${id}`;
    mysqlDb.query(sql, (err, results) => {
        if (err) {
            res.status(400).send({
                "error": `error occurred`
            });
        } else {
            //console.log(results);
            if (results.affectedRows == 0) {
                res.status(205).send({
                    "failed": `Id: ${id} menu does not exist`
                })
            } else {
                res.status(200).send({
                    "success": `Id: ${id} menu has been deleted`
                });
            }
        }
    });
}