const mysqlDb = require('../dbService');
const Joi = require('joi');
const adminRouleAuth = require('../security/adminRoleAuth');

// create the artices table
exports.createTable = async function (req, res){
    try{
        let sql = `CREATE TABLE articles (id int AUTO_INCREMENT PRIMARY KEY, content BLOB, title varchar(255), create_date DATETIME DEFAULT NULL,category varchar(255), views int(4) DEFAULT 0)`;
        const response = await new Promise((resolve, reject) => {
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message))
                resolve(results);
            });
        })
        .then(res.status(200).send({success: "Articles table created"}))
        .catch(err => res.status(400).send({error: "Error Occured"}))
    }catch(error){
        console.log(error);
    }
}

// create an article
exports.createArticle = async function (req, res){
    try{
        const title = req.body.title,
             content = req.body.content,
             category = req.body.category;
        
        if (!adminRouleAuth.authorizeAdminRole(req.body.role, category)){
            console.log(`The Access is Restricted for Admin of ${req.body.role}`);
            res.status(400).send({error: `The Access is Restricted for Admin of ${req.body.role}`});
            return;
        }

        const schema = Joi.object({
            content: Joi.string().required(),
            title: Joi.string().required(),
            category: Joi.string().required(),
            role: Joi.string().required()
        });
        const result = schema.validate(req.body);
        if (result.error){
            console.log(result.error.details[0].message);
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const response = await new Promise((resolve, reject) => {
            mysqlDb.query(`SELECT * FROM articles WHERE title = '${title}'`, (error, result) => {
                if (error) reject(new Error(error.message));

                if (result.length != 0){
                    console.log("the title is exist")
                    res.status(400).send({error: "the title is exist"})
                    return;
                }

                var sql = `INSERT INTO articles (content, title, category)  VALUES ('${content}', '${title}', '${category}');`;
                mysqlDb.query(sql, (err, results) => {
                    if(err) reject(new Error(err.message));

                    if (!results || results.affectedRow < 1) {
                        reject(() => {
                            res.status(400).send({
                                error: "Failed to add Article"
                            });
                            return;
                        })
                    }
                    resolve(results);
                })
            })
        }).then(data => {
                res.status(200).send({
                    success: "Article added",
                    content: content,
                    title: title,
                    category: category
                });
            })
            .catch(err => {
                res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
                console.log(err)
            });
    }catch(err) {
        if (err){
            console.log(err);
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
        }
    }
}

// get all articles
exports.getAllArticles = async function (req, res) {
    try{
        const response = await new Promise((resolve, reject) => {
            const sql = "SELECT id, title, CONVERT(content USING utf8) content, category FROM articles";

            mysqlDb.query(sql, (err,results) => {
                if (err) reject (new Error(err.message));
                resolve(results);
            });
        })
        .then(data => {
           res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: "Error occured"
            })
            console.log(err);
        })
    }catch(err){
        if (err){
            console.log(err);
            res.status(500).send({
                error: "Error occured"
            });
        }
    }
}

// get articles by category
exports.getArticlesByCategory = async function (req, res) {
    try {
        const category = req.params.category;

        const schema = Joi.object({
            category: Joi.string().required()
        });
        const result = schema.validate(req.params);
        if (result.error){
            console.log(result.error.details[0].message);
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const reponse = await new Promise((resolve, reject) => {
            var sql = `SELECT id, title, CONVERT(content USING utf8) content, category FROM articles WHERE category = '${category}'`

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(err);
                resolve(results);
            })
        }).then(data => res.json(data)).catch(err => {
            res.status(500).send({error: "Error Occured"});
            console.log(err)
        });

    } catch (err) {
        if (err) {
            res.status(500).send({error: "Error Occured"});
            console.log(err);
        }
    }
}


// get article by title
exports.getArticleByTitle = async function(req, res){
    try {
        const title = req.params.title;

        const schema = Joi.object({
            title: Joi.string().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const reponse = await new Promise((resolve, reject) => {
            var sql = `SELECT id, title, category,CONVERT(content USING utf8) content FROM articles WHERE title = '${title}'`

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(err);
                resolve(results);
            })
        }).then(data => res.json(data)).catch(err => {
            res.status(500).send({error: "Error Occured"});
            console.log(err)
        });

    } catch (err) {
        if (err) {
            res.status(500).send({error: "Error Occured"});
            console.log(err);
        }
    }
}

// get article by title
exports.getArticleById = async function(req, res){
    try {
        const id = req.params.id;

        const schema = Joi.object({
            id: Joi.string().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const reponse = await new Promise((resolve, reject) => {
            var sql = `SELECT id, title, category,CONVERT(content USING utf8) content FROM articles WHERE id = '${id}'`

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(err);
                resolve(results);
            })
        }).then(data => res.json(data)).catch(err => {
            res.status(500).send({error: "Error Occured"});
            console.log(err)
        });

    } catch (err) {
        if (err) {
            res.status(500).send({error: "Error Occured"});
            console.log(err);
        }
    }
}

// delete article by id
exports.deleteArticleById = async function(req, res){
    try {
        const id = req.params.id;
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            console.log(result.error.details[0].message);
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

    
        const response = await new Promise((resolve, reject) => {
           const query = `DELETE FROM articles WHERE id = '${id}'`;
           mysqlDb.query(query, (err, results) => {
               if (err) reject(new Error(err.message));
               resolve(results);
           });
        })
        .then(data => data.affectedRows == 0 ? res.status(205).send({error:'Item Does not exist'}) : res.status(200).send({success:' Item Deleted'}))
        .catch(err => res.status(400).send({error: `Error Occured`}));
    } catch (error){
        console.log(error);
    }
}

// edit article by id
exports.editArticleById = async function(req, res){
    try{
        const content = req.body.content,
            title = req.body.title,
            category = req.body.category
        const id = req.params.id;
        if (!id){
            res.send({
                status: false,
                message: "you should provide valid id when editing",
            });
            return;
        }
        if (!adminRouleAuth.authorizeAdminRole(req.body.role, category)){
            console.log(`The Access is Restricted for  Admin of ${req.body.role}`);
            res.status(400).send({error: `The Access is Restricted for Admin of ${req.body.role}`});
            return;
        }

        const schema = Joi.object({
            content: Joi.string().required(),
            title: Joi.string().required(),
            category: Joi.string().required(),
            role: Joi.string().required()
        });
        const result = schema.validate(req.body);
        if (result.error) {
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const response = await new Promise((resolve, reject) => {
            var sql = `UPDATE articles SET content = '${content}', title = '${title}', category ='${category}' WHERE id = ${id}`;

            mysqlDb.query(sql, (err, results) => {
                if (err)
                    reject(new Error(err.message));
                console.log(results);
                resolve(results);
            });
            // mysqlDb.query('SELECT LAST_INSERT_ID() id', (err, result) => {
            //     if (err)
            //         reject(new Error(err.message));
            //     resolve(result);
            // })
        }).then(data => {
            if (data.affectedRows < 1) {
                res.status(400).send({
                    error: "Cannot find the article. Failed to edit"
                });
            }else {
                res.status(200).send({
                    //event_id: data[0].event_id,
                    success: "Edit successfully",
                    content: content,
                    title: title,
                    category: category
                });
            }
        })
        .catch(err => {
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
            console.log(err)
        });
    }catch (err){
        if (err){
            console.log(err);
            res.status(500).send({error: "error: invalid input; your input may contain invalid character"});
        }
    }
}