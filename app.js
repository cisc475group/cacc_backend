const Joi = require('joi');
const express = require('express');
const app = express();
const cors = require('cors');    // not sure if we need, if it is uncommon and npm install cors
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const morgan = require('morgan');
dotenv.config();

const dbService = require('./dbService');
const mailService = require('./mailService');
const passportService = require('./security/passport');

app.use(passportService.passport.initialize());

app.use(express.json());
//app.use(express.urlencoded({extended: false}));
 
// static files
app.use(fileUpload({
    createParentPath: true,
    limits: { 
        fileSize: 2 * 1024 * 1024 * 1024 //2MB max file(s) size
    },
}));

const corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
  
app.use('/static',cors(corsOptions), express.static('uploads'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));

// PORT
const port = process.env.PORT || 3000;
app.listen(port, ()=>{
    console.log(`Listening on port ${port}...`);
    console.log(`URL: ${process.env.PORT ? "https://caccback.herokuapp.com/" : 
    'http://localhost:3000/'}`);
});



// serving static files
app.use(express.static('images')); 


/*******************************************/
// Initializing route group
/*******************************************/
const apiRoutes = express.Router();

// const visitRouter = require('./routes/visit');
const headerMenuRouter = require('./routes/headerMenu');
const adminsRouter = require('./routes/admins');
const authRouter = require('./routes/authentication');
const uploadRouter = require('./routes/upload');
const eventsRouter = require('./routes/events');
const clubRouter = require('./routes/clubs');
const articleRouter = require('./routes/articles');
const mailServiceRouter = require('./routes/mailService');

// all mailService routes are in the mailServiceRouter
apiRoutes.use('/mailService', mailServiceRouter);

// all articles routes are in the articleRouter
apiRoutes.use('/articles', articleRouter);

// all auth routes are in the authRouter
apiRoutes.use('/auth', authRouter);

// all headerMenu routes are in the headerMenuRouter
apiRoutes.use('/headerMenu', headerMenuRouter);

// all admins routes are in the adminsRouter
apiRoutes.use('/admins', adminsRouter);

// all files routes are in the fileRouter
apiRoutes.use('/files', uploadRouter);

// all events routes are in the eventRouter
apiRoutes.use('/events', eventsRouter);

// all club routes are in the clubRouter
apiRoutes.use('/club', clubRouter);

// all api routes are in apiRoutes
app.use('/api', apiRoutes);




